# BSPWM DOTFILES

![preview](./preview.png)

![preview](./preview_2.png)

![preview](./preview_3.png)

## Install 

This rice use these following packages :

| Package       | Role                | Pacman  | Yay |
|---------------|---------------------|---------|-----|
| bat           | better cat          | x       |     |
| bspwm         | window manager      | x       |     |
| exa           | better ls           | x       |     |
| feh           | image tool          | x       |     |
| flameshot     | screenshot tool     | x       |     |
| git           | repo version control | x       |     |
| alacritty     | terminal            | x       |     |
| lightdm       | X display manager   | x       |     |
| neovim        | code editor         | x       |     |
| picom-git     | X compositor        |         | x   |
| polybar       | the B A R           | x       |     |
| rofi          | program launcher    | x       |     |
| spotify-git   | spotify client      |         | x   |
| spicetify-cli | spotify client tool |         | x   |
| sxhkd         | X hotkey deamon     | x       |     |

###### Download and install dependencies

```shell
~ $ sudo pacman -Syy alacritty bat bspwm exa feh flameshot git lightdm neovim polybar rofi sxhkd
~ $ yay -S picom-git spotify-git spicetify-cli
```

###### Download and install dotfiles

- clone the repo

```~ $ git clone https://gitlab.com/AtiPique/bspwm-dotfiles```

-  and then copy the files to the right folders

```shell
~ $ cp -r bspwm_dotfiles/.config ~/
~ $ cp -r bspwm_dotfiles/.bashrc ~/
~ $ sudo cp -r bspwm_dotfiles/JetBrainsMono /usr/share/fonts/
```

## Hotkeys 
| Shortcut                 | What it does                |
| -------------------------| ----------------------------|
| super + w                | Close window                |
| super + d                | Open rofi (app launcher)    |
| super + return           | Open kitty terminal         |
| super + b                | Open firefox                |
| super + tab              | Switch monocle/tiled mode   |
| super + t                | Switch to tiled mode        |
| super + f                | Switch to fullscreen mode   |
| super + space            | Switch to floating mode     |
| super + ctrl + {h,j,k,l} | Move a floating window      |
| super + alt + {h,j,k,l}  | Resize active window        |
| super + p                | reload polybar              |
| super + escape           | reload sxhkd and bspwm      | 
| super + shift + s        | screenshot                  |

## Credit

Fuyutaa's dotfiles : https://github.com/fuyutaaProjects/fuyutaa-dotfiles/

NvChad : https://github.com/NvChad/NvChad

## Licence 

![gplv3](./gplv3.png)
