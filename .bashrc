#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Alias for replace commands by others 

alias ls='exa --color=auto --icons'
alias la='exa --color=auto --icons -la'
alias cat='bat --color=auto'
alias grep='grep --color=auto'
alias nv='nvim'
alias py='python3'
alias emacs='emacs -nw'
alias usb1='cd /run/media/atipique_/241B-9275/' 

# Custom prompt text
PS1=" ~  "

export EDITOR=nv 
. "$HOME/.cargo/env"
# >>> xmake >>>
test -f "/home/atipique_/.xmake/profile" && source "/home/atipique_/.xmake/profile"
# <<< xmake <<<
