#!/bin/bash

chown -R atipique_:atipique_ /sys/class/backlight/intel_backlight/brightness

echo "Polybar can now modify the screen brightness"
